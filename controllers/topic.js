'use strict'

var validator = require('validator');
var Topic = require('../models/topic');


var controller = {


save: function(req, res){
		
		//Recoger los parametros
		var params = req.body;
		//Validar los datos
		try{
			var validate_title = !validator.isEmpty(params.title);
			var validate_content = !validator.isEmpty(params.content);
			var validate_lang = !validator.isEmpty(params.lang);


		}catch(err){
			return res.status(200).send({
				message: 'Faltan Datos por enviar'
			});
		}

		if(validate_title && validate_content & validate_lang){
		//Crear objeto y guardar
		var topic = new Topic();

		//Asignar valores
		topic.title = params.title;
		topic.content = params.content;
		topic.code = params.code;
		topic.lang = params.lang;
		topic.user = req.user.sub;


		//Guardar el topic
		topic.save((err, topicStored) =>{

			
			if(err || !topicStored){
				return res.status(400).send({
				status: 'error',
				message: 'El tema no se ha guardado'
				});

			}


			//Devolver una respuesta
			return res.status(200).send({
			status: 'success',
			topic: topicStored
});


		});


	
		}else{
			return res.status(400).send({
			message: 'Los datos no son validos'
			});
		}
		
},

getTopics: function(req, res){
		
		//Cargar la libreria de paginacion en la clase(Modelo)

		//Recoger la pagina actual
		if(!req.params.page || req.params.page == 0 || req.params.page == '0' || req.params.page == null || req.params.page == undefined){
			var page = 1;
		}else{
			var page = parseInt(req.params.page);
		}
		
		//Indica las opciones de paginacion
		var options ={

			sort: { date: -1 },
			populate: 'user',
			limit: 5,
			page: page
		};
		//Find paginado
		Topic.paginate({}, options, (err, topics) =>{


			if(err){
				return res.status(500).send({
				status: 'error',
				message: 'Error al hacer la consulta'
			});
			}
			if(!topics){
				return res.status(404).send({
				status: 'error',
				message: 'No hay topics creados'
			});
			}

			//Devolver Resultado (Topics, total de topics, total de paginas)
				return res.status(200).send({
				status: 'success',
				topics: topics.docs,
				totalDocs: topics.totalDocs,
				totalPages: topics.totalPages
			});
		
		});
		
},

getTopicsByUser: function(req, res){

	//Conseguir el id de usuario
	var userId = req.params.user;
	//Find con una condicion de ususario
	Topic.find({

		user: userId
	})
	.sort([['date' , 'descending']])
	.exec((err, topics) =>{

		if(err){
			//Devolver Resultado
			return res.status(500).send({
				status: 'error',
				message: 'Error en la peticion'
	});
		}

		if(!topics){
			//Devolver Resultado
				return res.status(404).send({
				status: 'error',
				message: 'No hay temas para mostrar'
			});

		}
			//Devolver Resultado
		return res.status(200).send({
				status: 'success',
				topics
		});

	});
	
},

getTopic: function(req, res){


		//Sacar el id de topic de la url
		var topicId = req.params.id;
		//find por id de topic
		Topic.findById(topicId)
			 .populate('user')
			 .exec((err, topic) =>{

			 	if(err){
			 		return res.status(500).send({
						status: 'error',
						message:'Error en la peticion'
					});

			 	}	

			 	if(!topic){
			 		return res.status(404).send({
						status: 'error',
						message:'No hay topic para mostrar'
					});

			 	}
					//Devolver Resultado
					return res.status(200).send({
						status: 'success',
						topic
					});
			 });
		
},

update: function(req, res){


	//Recoger el id del topic de la url
	var topicId = req.params.id;

	//Recoger datos que llegan del post
	var params = req.body;
	//Validar Datos
	try{
			var validate_title = !validator.isEmpty(params.title);
			var validate_content = !validator.isEmpty(params.content);
			var validate_lang = !validator.isEmpty(params.lang);


		}catch(err){
			return res.status(200).send({
				message: 'Faltan Datos por enviar'
			});
		}
	
	if(validate_title && validate_content && validate_lang){
		//Mostrar un json con los datos modificables
		var update = {
			title: params.title,
			content: params.content,
			code: params.code,
			lang: params.lang
		};
		//find and update del topic por id y por id de ususario
		Topic.findOneAndUpdate({_id: topicId, user: req.user.sub}, update, {new:true}, (err, topicUpdate) => {

			if(err){
				return res.status(500).send({
					status: 'error',
					message: 'Error en la peticion'
				});
		}
			

			if(!topicUpdate){
				return res.status(404).send({
					message: 'No se ha podido actualizar el tema'
				});
			}
			

		//Devolver una respuesta
			return res.status(200).send({
			message: 'success',
			topic: topicUpdate
			});


		});
	

	}else{
		//Devolver una respuesta
		return res.status(200).send({
		message: 'La validacion de los datos no es correcta'
		});

	}
},

delete: function(req, res){

	//Sacar el id de topic de la url
	var topicId = req.params.id;
	//find al delete por topic id y por user id
	Topic.findOneAndDelete({_id: topicId, user: req.user.sub}, (err, topicRemoved) =>{

			if(err){
				return res.status(500).send({
				message: 'Error a la peticion'
				});
			}

			if(!topicRemoved){
				return res.status(404).send({
				message: 'No se ha podido eliminar el tema'
				});
			}


			//Devolver una respuesta

			return res.status(200).send({
			status: 'success',
			topic: topicRemoved
			});

	});
	
},

search: function(req,res){

	//Sacar el string a buscar de la url
	var searchString = req.params.search;
	//find or
	Topic.find({ "$or": [
		
		{"title": {"$regex": searchString, "$options": "i"}},
		{"content": {"$regex": searchString, "$options": "i"}},
		{"code": {"$regex": searchString, "$options": "i"}},
		{"lang": {"$regex": searchString, "$options": "i"}}
	]})
	.sort([['date' , 'descending']])
	.exec((err, topics) => {

		if(err){
			return res.status(500).send({
				status: 'Error',
				message:'Error en la peticion'
			});
		}

		if(!topics){
			return res.status(404).send({
				status: 'Error',
				message:'No hay temas disponible'
			});
		}
			//Devolver resultado
		return res.status(200).send({
			status: 'success',
			topics

		});

	});
	
}

};

module.exports = controller;