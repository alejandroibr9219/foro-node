'use strict'

var Topic = require('../models/topic');
var validator = require('validator');

var controller = {

   add: function(req, res){
   	//Recoger el id de topi de la url
   	var topicId = req.params.topicId;
   	//Find del id de topic
   	Topic.findById(topicId).exec((err, topic) => {

 
   	if(err){
   		return res.status(500).send({
   		status: 'error',
   		message:'Error en la peticion'
   		});
   	}

   	if(!topic){
   		return res.status(404).send({
   		status: 'error',
   		message:'El tema  no existe'
   		});
   	}

   	//Coprobar obj usuario y validar datos
   	if(req.body.content){
   		try{
			var validate_content = !validator.isEmpty(req.body.content);


		}catch(err){
			return res.status(200).send({
				message: 'No has comentado nada!!'
			});
		}

	if(validate_content){

	var comment = {
		user: req.user.sub,
		content: req.body.content
	};
	//En la priedad comment de obj resultante hacer un push
	topic.comments.push(comment);
   	//Guardar el topic completo
   	topic.save((err) => {


   	if(err){
		   		return res.status(500).send({
		   		status: 'error',
		   		message:'Error al guardar el comentario'
		   		});
		   	}
 	//Devolver una respuesta

			   	return res.status(200).send({
			   		status: 'success',
			   		topic
			   		});


   	});
  
	}else{

				return res.status(500).send({
		   		message:'No se han validado los datos de comentario'
		   		});
		   	
		}		
   	}
 
   	});
  

   },

      update: function(req, res){
      	//Conseguir el ide del comentario que llega por la url
      	var commentId = req.params.commentId;
      	//Recoger datos y validar
      	var params = req.body;
      	//Validar Datos
      	try{
			var validate_content = !validator.isEmpty(params.content);


		}catch(err){
			return res.status(200).send({
				message: 'No has comentado nada!!'
			});
		}

		if(validate_content){

		//FindAndUpdate de sub documento de comentarios
		Topic.findOneAndUpdate( 
			{"comments._id": commentId},
			 {
			 	"$set":{
			 		"comments.$.content": params.content
			 	}
			 },
			 {new:true},
			 (err, topicUpdate) => {
			 		 	if(err){
					   		return res.status(500).send({
					   		status: 'error',
					   		message:'Error en la peticion'
					   		});
			   	}

					   	if(!topicUpdate){
					   		return res.status(404).send({
					   		status: 'error',
					   		message:'El tema  no existe'
					   		});
					   	}
			 			//Devolver datos
					   	return res.status(200).send({
					   		status: 'success',
					   		topic: topicUpdate
					   	});
			 });
      
		}
      

   },

      delete: function(req, res){

      	//Sacar el id de topic y comentario a borrar que lelga por la url
      	var topicId = req.params.topicId;
      	var commentId = req.params.commentId;
      	//Buscar el topic
      	Topic.findById(topicId, (err, topic) => {
      		if(err){
					   		return res.status(500).send({
					   		status: 'error',
					   		message:'Error en la peticion'
					   		});
			   	}

					   	if(!topic){
					   		return res.status(404).send({
					   		status: 'error',
					   		message:'El tema  no existe'
					   		});
					   	}

      	//Seleccionas el subdocumento
      	var comment = topic.comments.id(commentId);
      	//Borrar el comentario
      	if(comment){
      		comment.remove();

      		//Guardar el topic
      		topic.save((err) => {
      			if(err){
					   		return res.status(500).send({
					   		status: 'error',
					   		message:'Error en la peticion'
					   		});
			   	}
			   	//Devolver un resultado

			   	return res.status(200).send({
			   		status: 'success',
			   		topic
			   	});
      		});
      		
      	}else{
      		return res.status(404).send({
			status: 'error',
			message:'El comentario  no existe'
			});
      	}
      	
    });
     

   }
};

module.exports = controller;