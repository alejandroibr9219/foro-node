'use strict'

var mongoose = require('mongoose');
var mongoosePaginate = require('mongoose-paginate-v2');
var Schema = mongoose.Schema;

//MODEL DE COMMENTS

var CommentSchema = Schema({
	content: String,
	date: {type: Date, default: Date.now },
	user: {type: Schema.ObjectId, ref: 'User' }

});

var Comment = mongoose.model('Comment', CommentSchema);


//MODEL DE TOPIC
var TopicSchema = Schema({
	title: String,
	content: String,
	code: String,
	lang: String,
	date: {type: Date, default: Date.now },
	user: {type: Schema.ObjectId, ref: 'User' },
	comments: [CommentSchema]

});

//Carga paginacion
TopicSchema.plugin(mongoosePaginate);

module.exports = mongoose.model('Topic', TopicSchema);
					//lowercase y pluralizar el nombre
					//users-> documentos(Schema)