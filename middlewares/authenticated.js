'use strict'

var secret = 'clave-secreta-para-generar-el-token-9999';
var jwt = require('jwt-simple');
var moment = require('moment');


exports.authenticated= function(req, res, next){

	//COmprobar sui llega autorization
	if(!req.headers.authorization){
		return res.status(403).send({
			message: 'La peticion no tiene la cabecera de autorization'
		});
	}
	//Limpiar token y quitar comillas
	var token = req.headers.authorization.replace(/['"]+/g, '');

	try{
	// Decodificar token
	var payload = jwt.decode(token, secret);

	// Comprobar si el token a expirado
	if( payload.exp <= moment().unix()){
		return res.status(404).send({
			message: 'El token ha expirado'
		});

	}

	}catch(ex){
		return res.status(404).send({
			message: 'El token no es valido'
		});
	}

	//Adjuntar usuario identificado a requet
	req.user = payload;
	//Pasr a la accion

	next();
};